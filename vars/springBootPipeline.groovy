def call() {

    timestamps {
        pipeline {
            agent any

            options {
                buildDiscarder(logRotator(numToKeepStr: '5'))
            }

            stages {
                stage('Cleaning docker') {
                    when {
                        anyOf {
                            branch 'develop'
                        }
                    }
                    steps {
                        sh "docker stop hellocicd-container || true"
                        sh "docker rm hellocicd-container || true"
                    }
                }
                stage('Maven compilation') {
                    steps {
                        script {
                            sh "mvn clean -U package"
                        }
                    }
                }
                stage('Building with Docker') {
                    when {
                        anyOf {
                            branch 'develop'
                        }
                    }
                    steps {
                        script {
                            sh "docker build -t hellocicd ."
                        }
                    }
                }
                stage('Running with docker') {
                    when {
                        anyOf {
                            branch 'develop'
                        }
                    }
                    steps {
                        sh "docker run -p 9090:8080 --name hellocicd-container -d hellocicd"
                    }
                }
            }
            // post {
            //     always {
            //         // sh 'docker system prune -f'
            //         // cleanWs disableDeferredWipeout: true, cleanWhenSuccess: true, cleanWhenFailure: true, cleanWhenAborted: true, deleteDirs: true
            //     }
            // }
        }
    }

}